package com.example.mango.admin.service.impl;


import java.util.List;

import com.example.mango.admin.dao.SysUserRoleMapper;
import com.example.mango.admin.model.SysUserRole;
import com.example.mango.admin.service.SysUserRoleService;
import com.example.mango.core.page.MybatisPageHelper;
import com.example.mango.core.page.PageRequest;
import com.example.mango.core.page.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public int save(SysUserRole record) {
        if (record.getId() == null || record.getId() == 0) {
            return sysUserRoleMapper.insertSelective(record);
        }
        return sysUserRoleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int delete(SysUserRole record) {
        return sysUserRoleMapper.deleteByPrimaryKey(record.getId());
    }

    @Override
    public int delete(List<SysUserRole> records) {
        for (SysUserRole record : records) {
            delete(record);
        }
        return 1;
    }

    @Override
    public SysUserRole findById(Long id) {
        return sysUserRoleMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult findPage(PageRequest pageRequest) {
        Object label = pageRequest.getParam("label");
        if (label != null) {
            return MybatisPageHelper.findPage(pageRequest, sysUserRoleMapper, "findPageByLabel", label);
        }
        return MybatisPageHelper.findPage(pageRequest, sysUserRoleMapper);
    }


}
