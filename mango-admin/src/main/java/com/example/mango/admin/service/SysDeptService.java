package com.example.mango.admin.service;

import com.example.mango.admin.model.SysDept;
import com.example.mango.core.service.CurdService;

import java.util.List;

//public interface SysUserService {
public interface SysDeptService extends CurdService<SysDept> {


    /**
     * 查询机构树
     * @param
     * @return
     */
    List<SysDept> findTree();
}
