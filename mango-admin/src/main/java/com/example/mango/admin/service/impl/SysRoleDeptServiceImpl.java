package com.example.mango.admin.service.impl;


import java.util.List;

import com.example.mango.admin.dao.SysRoleDeptMapper;
import com.example.mango.admin.model.SysRoleDept;
import com.example.mango.admin.service.SysRoleDeptService;
import com.example.mango.core.page.MybatisPageHelper;
import com.example.mango.core.page.PageRequest;
import com.example.mango.core.page.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRoleDeptServiceImpl implements SysRoleDeptService {

    @Autowired
    private SysRoleDeptMapper sysRoleDeptMapper;

    @Override
    public int save(SysRoleDept record) {
        if (record.getId() == null || record.getId() == 0) {
            return sysRoleDeptMapper.insertSelective(record);
        }
        return sysRoleDeptMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int delete(SysRoleDept record) {
        return sysRoleDeptMapper.deleteByPrimaryKey(record.getId());
    }

    @Override
    public int delete(List<SysRoleDept> records) {
        for (SysRoleDept record : records) {
            delete(record);
        }
        return 1;
    }

    @Override
    public SysRoleDept findById(Long id) {
        return sysRoleDeptMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult findPage(PageRequest pageRequest) {
        Object label = pageRequest.getParam("label");
        if (label != null) {
            return MybatisPageHelper.findPage(pageRequest, sysRoleDeptMapper, "findPageByLabel", label);
        }
        return MybatisPageHelper.findPage(pageRequest, sysRoleDeptMapper);
    }


}
