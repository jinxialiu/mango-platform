package com.example.mango.admin.dao;

import com.example.mango.admin.model.SysConfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author jinxia
 * @since 2020-03-19
 */

public interface SysConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysConfig record);

    int insertSelective(SysConfig record);

    SysConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysConfig record);

    int updateByPrimaryKey(SysConfig record);

    List<SysConfig> findPage();

    List<SysConfig> findPageByLabel(@Param(value = "label") String label);

    List<SysConfig> findByLable(@Param(value = "label") String label);
}