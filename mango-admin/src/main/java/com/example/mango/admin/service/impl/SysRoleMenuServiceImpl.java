package com.example.mango.admin.service.impl;


import java.util.List;

import com.example.mango.admin.dao.SysRoleMenuMapper;
import com.example.mango.admin.model.SysRoleMenu;
import com.example.mango.admin.service.SysRoleMenuService;
import com.example.mango.core.page.MybatisPageHelper;
import com.example.mango.core.page.PageRequest;
import com.example.mango.core.page.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public int save(SysRoleMenu record) {
        if (record.getId() == null || record.getId() == 0) {
            return sysRoleMenuMapper.insertSelective(record);
        }
        return sysRoleMenuMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int delete(SysRoleMenu record) {
        return sysRoleMenuMapper.deleteByPrimaryKey(record.getId());
    }

    @Override
    public int delete(List<SysRoleMenu> records) {
        for (SysRoleMenu record : records) {
            delete(record);
        }
        return 1;
    }

    @Override
    public SysRoleMenu findById(Long id) {
        return sysRoleMenuMapper.selectByPrimaryKey(id);
    }

    @Override
    public PageResult findPage(PageRequest pageRequest) {
        Object label = pageRequest.getParam("label");
        if (label != null) {
            return MybatisPageHelper.findPage(pageRequest, sysRoleMenuMapper, "findPageByLabel", label);
        }
        return MybatisPageHelper.findPage(pageRequest, sysRoleMenuMapper);
    }


}
