package com.example.mango.admin.service;

import com.example.mango.admin.model.SysConfig;
import com.example.mango.core.service.CurdService;

import java.util.List;

//public interface SysUserService {
public interface SysConfigService extends CurdService<SysConfig> {
    /**
     * 根据名称查询
     * @param lable
     * @return
     */
    List<SysConfig> findByLable(String lable);

}
