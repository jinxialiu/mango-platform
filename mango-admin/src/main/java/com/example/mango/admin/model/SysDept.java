package com.example.mango.admin.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SysDept implements Serializable {
    private Long id;

    private String name;

    private Long parent_id;

    private Integer order_num;

    private String create_by;

    private Date create_time;

    private String last_update_by;

    private Date last_update_time;

    private Byte del_flag;

    private static final long serialVersionUID = 1L;
    // 非数据库字段
    private List<SysDept> children;
    // 非数据库字段
    private String parentName;
    // 非数据库字段
    private Integer level;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<SysDept> getChildren() {
        return children;
    }

    public void setChildren(List<SysDept> children) {
        this.children = children;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getOrder_num() {
        return order_num;
    }

    public void setOrder_num(Integer order_num) {
        this.order_num = order_num;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by == null ? null : create_by.trim();
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getLast_update_by() {
        return last_update_by;
    }

    public void setLast_update_by(String last_update_by) {
        this.last_update_by = last_update_by == null ? null : last_update_by.trim();
    }

    public Date getLast_update_time() {
        return last_update_time;
    }

    public void setLast_update_time(Date last_update_time) {
        this.last_update_time = last_update_time;
    }

    public Byte getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(Byte del_flag) {
        this.del_flag = del_flag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", parent_id=").append(parent_id);
        sb.append(", order_num=").append(order_num);
        sb.append(", create_by=").append(create_by);
        sb.append(", create_time=").append(create_time);
        sb.append(", last_update_by=").append(last_update_by);
        sb.append(", last_update_time=").append(last_update_time);
        sb.append(", del_flag=").append(del_flag);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}